#!/usr/bin/env node

const client = require('prom-client');
const express = require('express');
const server = express();

// const counter = new client.Counter({
//   name: 'metric_name',
//   help: 'metric_help'
// });
// counter.inc(); // Inc with 1
// counter.inc(10); // Inc with 10

// client.collectDefaultMetrics();

server.get('/metrics', (req, res) => {
	res.set('Content-Type', client.register.contentType);
	res.end(client.register.metrics());
});

server.listen(3000);

const rate_total = new client.Counter({
  name: "rate_total",
  help: "total counting of rate",
  labelNames: ['prescale_column', 'fill']
});

const rate_estimated = new client.Gauge({
  name: "rate_expected",
  help: "what we think the current rate should be"
});

const rate_error = new client.Gauge({
  name: "rate_error",
  help: "applied error"
});

//const rate_estimate = new client.Gauge({
//  name: "rate_estimated",
//  help: "estimated current rate"
//});

//const future_rate_total = new client.Counter({
//  name: "rate_future_total",
//  help: "total counting of rate for next prescale"
//});

const rate_period=60;
function getRate(max, min, t) {
  return max - t*(max-min)/rate_period;
}

const fill=9000
let prescale_column=1;
let sec_counter = 0;
let error_percent = 0;
setInterval(() => {
  sec_counter=(sec_counter+1)%rate_period;
  const rate = getRate(90, 70, sec_counter);
  rate_total.inc({prescale_column,fill}, rate);
  rate_estimated.set(rate*(1-error_percent));
  rate_error.set(error_percent);
  //rate_estimate.set(getRate(90, sec_counter));
}, 1000);

setInterval(() => {
  prescale_column = (prescale_column+1)%50;
  error_percent = (Math.random()*.4)-.2;
}, rate_period*1000);
